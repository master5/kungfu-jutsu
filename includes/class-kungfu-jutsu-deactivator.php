<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://kunguthemes.ngaa.men
 * @since      1.0.0
 *
 * @package    Kungfu_Jutsu
 * @subpackage Kungfu_Jutsu/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Kungfu_Jutsu
 * @subpackage Kungfu_Jutsu/includes
 * @author     Kungfuthemes <kungfuthemes@gmail.com>
 */
class Kungfu_Jutsu_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
