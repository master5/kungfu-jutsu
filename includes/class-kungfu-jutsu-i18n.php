<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://kunguthemes.ngaa.men
 * @since      1.0.0
 *
 * @package    Kungfu_Jutsu
 * @subpackage Kungfu_Jutsu/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Kungfu_Jutsu
 * @subpackage Kungfu_Jutsu/includes
 * @author     Kungfuthemes <kungfuthemes@gmail.com>
 */
class Kungfu_Jutsu_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'kungfu-jutsu',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
