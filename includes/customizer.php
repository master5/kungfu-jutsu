<?php 

// if ( class_exists( 'Qf_Customize_Panel' ) ) {

	/**
	 * Social Share Control
	 *
	 * @return void
	 * @author Kungfuthemes
	 **/
	add_action( 'customize_register', 'kungfu_customizer_general_social_share' );
	function kungfu_customizer_general_social_share( $wp_customize ) {

		// -----------------------------------------
		// Customize Panel Options Fields          -
		// -----------------------------------------
		$panel = new Qf_Customize_Panel( $wp_customize, 
			array(
				'id' 			=> 'kungfu_customizer_addons_panel',
				'title' 		=> esc_html__( 'Addons Settings', 'kungfu-jutsu' ),
				'priority'		=> 5
			) 
		);

		$panel->add_section( array(
			'id' 			=> 'kungfu_social_share_section',
			'heading'		=> esc_attr__( 'Social Share Settings', 'kungfu-jutsu' ),
			'fields' 		=> array( //Fields in section
				array(
					'name' 		=> 'kungfu_social_share',
					'type' 		=> 'checkbox',
					'heading' 	=> esc_html__( 'Social Share', 'kungfu-jutsu' ),
					'value'		=> 0,
				),
			)
		) );

	}
// }