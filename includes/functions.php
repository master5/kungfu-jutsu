<?php 

/**
 * Add user meta field
 *
 * @return void
 **/
add_filter( 'user_contactmethods', 'kungfu_add_user_meta_field' );
function kungfu_add_user_meta_field( $profile_fields ) {
	// Add new fields
	$profile_fields['kungfu_facebook'] 		= esc_html__( 'Facebook URL', 'kungfu-jutsu' );
	$profile_fields['kungfu_twitter'] 		= esc_html__( 'Twitter URL', 'kungfu-jutsu' );
	$profile_fields['kungfu_pinterest'] 	= esc_html__( 'Pinterest URL', 'kungfu-jutsu' );
	$profile_fields['kungfu_instagram'] 	= esc_html__( 'Instagram URL', 'kungfu-jutsu' );

	return $profile_fields;
}

/**
 * Social Share
 *
 * @return void
 * @author Kungfuthemes
 **/
function kungfu_social_share() {
	global $post;

	if ( has_post_thumbnail() ) {
		$image 		= wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'large', false );
		$the_image 	= 'data-image="'.$image[0].'"';
	} else {
		$the_image 	= '';
	}
	ob_start();
	?>
	<div class="social-shares">
		<span class="shrd"><?php esc_html_e( 'SHARE THIS', 'kungfu-jutsu' ); ?></span>
		<div class="other-share kungfu-share" data-url="<?php the_permalink(); ?>" data-title="<?php the_title(); ?>" data-text="<?php echo strip_tags( $post->post_excerpt ); ?>" <?php echo ''.$the_image; ?> data-width="600" data-height="400">
			<a href="" class="facebook s_facebook"><i class="fa fa-fw fa-facebook"></i></a>
			<a href="" class="twitter s_twitter"><i class="fa fa-fw fa-twitter"></i></a>
			<a href="" class="google-plus s_plus"><i class="fa fa-fw fa-google-plus"></i></a>
			<a href="" class="linkedin s_linkedin"><i class="fa fa-fw fa-linkedin"></i></a>
		</div>
	</div>
	<?php 
	$share_html = ob_get_contents();
	ob_end_clean();

	$social_share = get_theme_mod( 'kungfu_social_share', 'no' );
	if ( 'yes' == $social_share ) {
		echo apply_filters( 'kungfu_social_share_html', $share_html );
	}
}