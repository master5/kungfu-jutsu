<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://kunguthemes.ngaa.men
 * @since             1.0.0
 * @package           Kungfu_Jutsu
 *
 * @wordpress-plugin
 * Plugin Name:       Kungfu Jutsu
 * Plugin URI:        http://kunguthemes.ngaa.men
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Kungfuthemes
 * Author URI:        http://kunguthemes.ngaa.men
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       kungfu-jutsu
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'KUNGFU_JUTSU_PATH', plugin_dir_path( __FILE__ ) );
define( 'KUNGFU_JUTSU_URL', plugin_dir_url( __FILE__ ) );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-kungfu-jutsu-activator.php
 */
function activate_kungfu_jutsu() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-kungfu-jutsu-activator.php';
	Kungfu_Jutsu_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-kungfu-jutsu-deactivator.php
 */
function deactivate_kungfu_jutsu() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-kungfu-jutsu-deactivator.php';
	Kungfu_Jutsu_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_kungfu_jutsu' );
register_deactivation_hook( __FILE__, 'deactivate_kungfu_jutsu' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-kungfu-jutsu.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_kungfu_jutsu() {

	$plugin = new Kungfu_Jutsu();
	$plugin->run();

}
run_kungfu_jutsu();
